package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"bitbucket.org/mattlagacebs/cre-common/dbutil"
	"bitbucket.org/mattlagacebs/cre-common/messagebroker"

	"github.com/dgraph-io/dgo"
	"github.com/dgraph-io/dgo/protos/api"
)

type worker struct {
	cqn  string                       // Endpoint Consumer queue name
	dbc  *dbutil.DBClient             // DB to get/insert provider data
	name string                       // Unique worker name
	mbp  *messagebroker.MessageBroker // Endpoint Publisher Message Broker
	mbc  *messagebroker.MessageBroker // Endpoint Consumer Message Broker
	pqn  string                       // Endpoint Publisher queue name
}

type endpoint struct {
	UID      string
	APIName  string
	DBSchema string
}

type provider struct {
	UID             string
	MappedSchema    string
	URL             string
	Type            string
	ServiceEndpoint []endpoint
}

type providers struct {
	Providers []provider
}

func (w worker) ProcessRequest(b []byte) {
	fmt.Printf("ProcessRequest msg received: %v\n", string(b))
	ed := strings.Split(string(b), "_")
	se := ""
	vars := map[string]string{}
	var d []byte

	if len(ed) > 1 {
		se = ed[1]
	}

	mbp := messagebroker.New(string(b) + "_result")

	switch ed[0] {
	case "delete":
		err := w.deleteData(vars)
		if err != nil {
			d = []byte("error:" + err.Error())
		} else {
			d = []byte{}
		}
		mbp.Publish(d)
		break
	case "create":
		// d = w.getSaveData(vars)
		break
	case "update":
		break
	case "get":
		mbp.Publish(w.getData(se))
		break
	default:
		//nothing to do
	}

	mbp.Close()
}

func (w worker) deleteData(vars map[string]string) error {
	mu := &api.Mutation{}
	dgo.DeleteEdges(mu, vars["id"])
	mu.CommitNow = true
	_, err := w.dbc.Client.NewTxn().Mutate(context.Background(), mu)
	if err != nil {
		return err
	}
	return nil
}

func (w worker) getData(se string) []byte {
	fmt.Printf("getData for service: %v\n", se)

	ed, err := w.getEndpointDataFromDB(se)

	if err != nil {
		fmt.Printf("Error retrieving Endpoint Data: %v\n", err)
		panic(err)
	}

	var r map[string]interface{}
	json.Unmarshal(ed, r)
	fmt.Printf("getEndpointDataFromDB: %v\n", r)

	if len(r) != 0 {
		return ed
	}

	err = w.getEndpointDataFromProviders(se)

	if err != nil {
		fmt.Printf("Error retrieving Endpoint Data from Providers: %v\n", err)
		panic(err)
	}

	d, err := w.getEndpointDataFromDB(se)

	if err != nil {
		fmt.Printf("Error retrieving Endpoint Data: %v\n", err)
		panic(err)
	}

	return d
}

func (w worker) getEndpointDataFromDB(se string) ([]byte, error) {
	const q = `
		query GetEndpointData($se: string) {
			data(func: has(service_endpoint)) @filter(eq(type, "data")) {
				service_endpoint @filter(uid($se)) {
					uid
				}
			}
		}
	`
	vars := map[string]string{"$se": se}
	d, err := w.dbc.Client.NewTxn().QueryWithVars(context.Background(), q, vars)

	if err != nil {
		return nil, err
	}
	return d.GetJson(), nil
}

func (w worker) getEndpointProviders(se string) ([]byte, error) {
	const q = `
		query GetEndpointProviders($se: string) {
			providers(func: eq(type, "provider")) @cascade {
				name
				service_endpoint @filter(uid($se)) {
					uid
				}
				uid
				url
			}
		}
	`
	vars := map[string]string{"$se": se}
	d, err := w.dbc.Client.NewTxn().QueryWithVars(context.Background(), q, vars)

	if err != nil {
		return nil, err
	}
	return d.GetJson(), nil
}

func (w worker) getEndpointDataFromProviders(se string) error {
	p, err := w.getEndpointProviders(se)

	if err != nil {
		return err
	}

	var r = &providers{
		Providers: []provider{},
	}
	json.Unmarshal(p, r)
	fmt.Printf("providers returned...: %+v\n", r)

	ps := make(chan string)

	for _, p := range r.Providers {
		go func(p provider) {
			resp, err := http.Get(p.URL)

			if err != nil {
				fmt.Printf("HTTP GET Request failed: %v\n", err)
			} else {
				defer resp.Body.Close()

				res, err := ioutil.ReadAll(resp.Body)

				if err != nil {
					fmt.Printf("Failed to read data JSON: %v", err)
				} else {
					fmt.Printf("Provider data received: %v\n", string(res))
					fmt.Printf("Provided data saved...: %v\n", len(res))
				}
			}
		}(p)
	}
	<-ps
	return nil
}

func (w worker) saveDataToDB(b []byte) {

}

func main() {
	w := worker{name: "requests_worker", dbc: dbutil.NewDBClient()}
	w.mbc = messagebroker.New("requests_queue")
	w.mbc.Consume(w)
	w.mbc.Close()
}
