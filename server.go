package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net"

	"bitbucket.org/mattlagacebs/cre-common/dbutil"
	"bitbucket.org/mattlagacebs/cre-common/messagebroker"
	"bitbucket.org/mattlagacebs/crepoc/crepb"

	"github.com/dgraph-io/dgo/protos/api"
	"google.golang.org/grpc"
)

type server struct{}

// Service Data Object
type Service struct {
	UID       string `json:"uid,omitempty"`
	APIName   string `json:"api_name"`
	Name      string `json:"name"`
	CreatedBy []User `json:"created_by"`
	Type      string `json:"type"`
}

// ServiceEndpoint Data Object
type ServiceEndpoint struct {
	UID       string    `json:"uid,omitempty"`
	APIName   string    `json:"api_name"`
	DBSchema  string    `json:"db_schema"`
	Name      string    `json:"name"`
	CreatedBy []User    `json:"created_by"`
	Service   []Service `json:"service"`
}

// ServiceEndpointProvider Data Object
type ServiceEndpointProvider struct {
	UID             string            `json:"uid,omitempty"`
	MappedSchema    string            `json:"mapped_schema,omitempty"`
	Name            string            `json:"name"`
	CreatedBy       []User            `json:"created_by"`
	ServiceEndpoint []ServiceEndpoint `json:"service_endpoint"`
	URL             string            `json:"url"`
	Type            string            `json:"type,omitempty"`
}

// User Data Object
type User struct {
	UID       string `json:"uid,omitempty"`
	Name      string `json:"name"`
	Email     string `json:"email,omitempty"`
	CreatedBy []User `json:"created_by"`
}

// ServiceEndpointDataProcessor implements MessageProcessor
type ServiceEndpointDataProcessor struct {
	se     string
	stream crepb.CREService_GetServiceEndpointDataServer
}

// ProcessRequest processes message received from worker queue
func (s *ServiceEndpointDataProcessor) ProcessRequest(r []byte) {
	fmt.Printf("Should stream service endpoint data: %v\n", r)
	res := &crepb.GetServiceEndpointDataResponse{
		Data: string(r),
	}
	s.stream.Send(res)
	fmt.Printf("Stream sent msg!")
}

func (*server) CreateService(ctx context.Context, req *crepb.CreateServiceRequest) (*crepb.CreateServiceResponse, error) {
	db := dbutil.NewDBClient()

	mu := &api.Mutation{
		CommitNow: true,
	}

	u := []User{}
	u = append(u, User{
		UID: req.GetCreatedBy(),
	})

	r := &Service{
		APIName:   req.GetApiName(),
		Name:      req.GetName(),
		CreatedBy: u,
		Type:      "service",
	}

	fmt.Printf("Service: %+v\n", r)

	b, err := json.Marshal(r)

	if err != nil {
		return nil, err
	}

	db.Client.Alter(context.Background(), &api.Operation{
		Schema: `
			type: string @index(term) .
			api_name: string @index(term) .
		`,
	})

	mu.SetJson = b
	assigned, err := db.Client.NewTxn().Mutate(ctx, mu)

	if err != nil {
		return nil, err
	}
	return &crepb.CreateServiceResponse{
		Uid:       assigned.Uids["blank-0"],
		ApiName:   r.APIName,
		Name:      r.Name,
		CreatedBy: r.CreatedBy[0].UID,
		Type:      r.Type,
	}, nil
}

func (*server) CreateServiceEndpoint(ctx context.Context, req *crepb.CreateServiceEndpointRequest) (*crepb.CreateServiceEndpointResponse, error) {
	if len(req.Service) == 0 {
		return nil, errors.New("Service param is required")
	}

	db := dbutil.NewDBClient()

	db.Client.Alter(context.Background(), &api.Operation{
		Schema: `
			type: string @index(term) .
			api_name: string @index(term) .
		`,
	})

	mu := &api.Mutation{
		CommitNow: true,
	}

	r := &ServiceEndpoint{
		APIName:  req.GetApiName(),
		Name:     req.GetName(),
		DBSchema: req.GetDbSchema(),
		CreatedBy: []User{{
			UID: req.GetCreatedBy(),
		}},
		Service: []Service{{
			UID: req.GetService(),
		}},
	}

	fmt.Printf("Service Endpoint: %+v\n", r)

	b, err := json.Marshal(r)
	fmt.Printf("after marshall err: %v\n", err)
	if err != nil {
		return nil, err
	}

	mu.SetJson = b
	assigned, err := db.Client.NewTxn().Mutate(ctx, mu)
	fmt.Printf("after mutate err: %v\n", err)
	if err != nil {
		return nil, err
	}
	return &crepb.CreateServiceEndpointResponse{
		Uid:       assigned.Uids["blank-0"],
		ApiName:   r.APIName,
		Name:      r.Name,
		CreatedBy: r.CreatedBy[0].UID,
		Service:   r.Service[0].UID,
	}, nil
}

func (*server) CreateServiceEndpointProvider(ctx context.Context, req *crepb.CreateServiceEndpointProviderRequest) (*crepb.CreateServiceEndpointProviderResponse, error) {
	if len(req.GetServiceEndpoint()) == 0 {
		return nil, errors.New("Service param is required")
	}

	db := dbutil.NewDBClient()

	mu := &api.Mutation{
		CommitNow: true,
	}

	r := &ServiceEndpointProvider{
		Name: req.GetName(),
		CreatedBy: []User{{
			UID: req.GetCreatedBy(),
		}},
		ServiceEndpoint: []ServiceEndpoint{{
			UID: req.GetServiceEndpoint(),
		}},
		Type: "provider",
		URL:  req.GetUrl(),
	}

	fmt.Printf("Service Endpoint Provider: %+v\n", r)

	b, err := json.Marshal(r)
	fmt.Printf("after marshall err: %v\n", err)
	if err != nil {
		return nil, err
	}

	mu.SetJson = b
	assigned, err := db.Client.NewTxn().Mutate(ctx, mu)
	fmt.Printf("after mutate err: %v\n", err)
	if err != nil {
		return nil, err
	}
	return &crepb.CreateServiceEndpointProviderResponse{
		Uid:             assigned.Uids["blank-0"],
		Name:            r.Name,
		CreatedBy:       r.CreatedBy[0].UID,
		ServiceEndpoint: r.ServiceEndpoint[0].UID,
		Url:             r.URL,
	}, nil
}

func (s *server) GetServiceEndpointData(req *crepb.GetServiceEndpointDataRequest, stream crepb.CREService_GetServiceEndpointDataServer) error {
	if len(req.GetServiceEndpoint()) == 0 {
		return errors.New("ServiceEndpoint param is required")
	}

	fmt.Printf("GetServiceEndpointData request received: %v\n", req.GetServiceEndpoint())

	mp := &ServiceEndpointDataProcessor{
		se:     req.GetServiceEndpoint(),
		stream: stream,
	}
	mb := messagebroker.New("requests_queue")
	mb.Publish([]byte("get_" + req.GetServiceEndpoint()))
	mb = messagebroker.New("get_" + req.GetServiceEndpoint() + "_result")
	mb.Consume(mp)
	mb.Close()
	return nil
}

func (*server) CreateUser(ctx context.Context, req *crepb.CreateUserRequest) (*crepb.CreateUserResponse, error) {
	if len(req.GetName()) == 0 || len(req.GetEmail()) == 0 {
		return nil, errors.New("Name and Email required")
	}

	db := dbutil.NewDBClient()

	mu := &api.Mutation{
		CommitNow: true,
	}

	r := &User{
		Name:  req.GetName(),
		Email: req.GetEmail(),
	}

	fmt.Printf("User Endpoint: %+v\n", r)

	b, err := json.Marshal(r)

	if err != nil {
		return nil, err
	}

	mu.SetJson = b
	assigned, err := db.Client.NewTxn().Mutate(ctx, mu)

	if err != nil {
		return nil, err
	}
	return &crepb.CreateUserResponse{
		Uid:   assigned.Uids["blank-0"],
		Name:  r.Name,
		Email: r.Email,
	}, nil
}

func (s *server) GetServices(ctx context.Context, req *crepb.GetServicesRequest) (*crepb.GetServicesResponse, error) {
	db := dbutil.NewDBClient()
	q := `
		{
			all(func: eq(type, "service")) {
				expand(_all_)
			}
		}
	`
	resp, err := db.Client.NewTxn().Query(context.Background(), q)

	if err != nil {
		db.Close()
		return nil, err
	}

	var r map[string][]map[string]interface{}

	json.Unmarshal(resp.GetJson(), &r)

	db.Close()

	fmt.Printf("Services returned: %v\n", r)

	return &crepb.GetServicesResponse{
		Data: string(resp.GetJson()),
	}, nil
}

func main() {
	fmt.Println("Initiating Listen")
	lis, err := net.Listen("tcp", ":23000")

	if err != nil {
		fmt.Printf("Failed to listen: %v\n", err)
	}

	defer lis.Close()

	s := grpc.NewServer()
	crepb.RegisterCREServiceServer(s, &server{})

	if err := s.Serve(lis); err != nil {
		fmt.Printf("Failed to serve: %v\n", err)
	}

	fmt.Println("CRE Service started")
}
