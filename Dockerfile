FROM golang:1.11-alpine3.8

WORKDIR /go/src/bitbucket.org/mattlagacebs/crepoc

COPY . .

RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh

RUN go get -d -v ./...
RUN go install -v ./...

CMD ["go", "run", "server.go"]