package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"strings"
	"time"

	"bitbucket.org/mattlagacebs/crepoc/crepb"

	"google.golang.org/grpc"
)

func createService(c crepb.CREServiceClient, an string, cb string, n string) {
	r, err := c.CreateService(context.Background(), &crepb.CreateServiceRequest{
		ApiName:   an,
		CreatedBy: cb,
		Name:      n,
	})

	if err != nil {
		fmt.Printf("Failed to create service: %v\n", err)
	}

	fmt.Printf(`
	Response received: {
		uid: %s,
		name: %s,
		api_name: %s,
		created_by: %s,
	}`, r.GetUid(), r.GetName(), r.GetApiName(), r.GetCreatedBy())
}

func createServiceEndpoint(c crepb.CREServiceClient, an string, cb string, n string, s string, f []string) {
	r, err := c.CreateServiceEndpoint(context.Background(), &crepb.CreateServiceEndpointRequest{
		ApiName:   an,
		CreatedBy: cb,
		DbSchema:  strings.Join(f, ","),
		Name:      n,
		Service:   s,
	})

	if err != nil {
		fmt.Printf("Failed to create service endpoint: %v\n", err)
	}

	fmt.Printf(`
	Response received: {
		uid: %s,
		name: %s,
		api_name: %s,
		created_by: %s,
	}`, r.GetUid(), r.GetName(), r.GetApiName(), r.GetCreatedBy())
}

func createServiceEndpointProvider(c crepb.CREServiceClient, cb string, n string, se string, url string) {
	r, err := c.CreateServiceEndpointProvider(context.Background(), &crepb.CreateServiceEndpointProviderRequest{
		Name:            n,
		CreatedBy:       cb,
		ServiceEndpoint: se,
		Url:             url,
	})

	if err != nil {
		fmt.Printf("Failed to create service endpoint provider: %v\n", err)
	}

	fmt.Printf(`Response received: {
		uid: %s,
		name: %s,
		service_endpoint: %s,
		url: %s,
		}`, r.GetUid(), r.GetName(), r.GetServiceEndpoint(), r.GetUrl())
}

func createUser(c crepb.CREServiceClient, n string, e string) {
	r, err := c.CreateUser(context.Background(), &crepb.CreateUserRequest{
		Name:  n,
		Email: e,
	})

	if err != nil {
		panic(err)
	}

	fmt.Printf(`Response received: {
		uid: %s,
		name: %s,
		emaol: %s,
		}`, r.GetUid(), r.GetName(), r.GetEmail())
}

func getServiceEndpointData(c crepb.CREServiceClient, se string) {
	s, err := c.GetServiceEndpointData(context.Background(), &crepb.GetServiceEndpointDataRequest{
		ServiceEndpoint: se,
	})

	if err != nil {
		fmt.Printf("Failed calling GetServiceEndpointData: %v\n", err)
		panic(err)
	}

	fmt.Printf("getServiceEndpointData request sent and response received\n")

	go func() {
		time.Sleep(5 * time.Second)
		os.Exit(3)
	}()

	for {
		msg, err := s.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			fmt.Printf("Failed receiving GetServiceEndpointData stream: %v\n", err)
			panic(err)
		}

		fmt.Printf("msg: %v\n", string([]byte(msg.GetData())))

		var res map[string]interface{}
		err = json.Unmarshal([]byte(msg.GetData()), &res)

		fmt.Printf("res: %v\n", res)

		if err != nil {
			fmt.Printf("Failed to unmarshall service endpoint data: %v\n", err)
			panic(err)
		}

		fmt.Printf("Response from GetServiceEndpointData: %v\n", res)
		break
	}
}

func main() {
	cc, err := grpc.Dial("grpc:23000", grpc.WithInsecure())
	if err != nil {
		fmt.Printf("Failed to connect to CRE Service: %v\n", err)
	}

	defer cc.Close()

	c := crepb.NewCREServiceClient(cc)

	// createUser(c, "Matthew", "matt.lagace@binarystar.ca")
	// createService(c, "politica", "0x3056a", "Politica")
	// createServiceEndpoint(c, "politicians", "0x3056a", "Politicians", "0x30563", []string{"first_name", "last_name", "email", "current_riding_party", "current_riding_province", "image", "elected_office", "district", "office", "address", "tel"})
	// createServiceEndpointProvider(c, "0x3056a", "OpenNorth - Representatives", "0x3056c", "https://represent.opennorth.ca/representatives/?limit=1000")
	getServiceEndpointData(c, "0x3056c")
}
