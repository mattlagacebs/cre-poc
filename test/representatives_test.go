package test

import (
	"binarystar/politica/politicaclient"
	"testing"
)

func TestGetRepresentatives(t *testing.T) {
	reps, err := politicaclient.GetRepresentatives()
	t.Logf("Reps returned: %+v", reps)
	t.Logf("Error: %v", err)
}
