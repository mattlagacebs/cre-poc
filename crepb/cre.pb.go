// Code generated by protoc-gen-go. DO NOT EDIT.
// source: crepb/cre.proto

package crepb

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

import (
	context "golang.org/x/net/context"
	grpc "google.golang.org/grpc"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type CreateServiceRequest struct {
	ApiName              string   `protobuf:"bytes,1,opt,name=api_name,json=apiName,proto3" json:"api_name,omitempty"`
	CreatedBy            string   `protobuf:"bytes,2,opt,name=created_by,json=createdBy,proto3" json:"created_by,omitempty"`
	Name                 string   `protobuf:"bytes,3,opt,name=name,proto3" json:"name,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CreateServiceRequest) Reset()         { *m = CreateServiceRequest{} }
func (m *CreateServiceRequest) String() string { return proto.CompactTextString(m) }
func (*CreateServiceRequest) ProtoMessage()    {}
func (*CreateServiceRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_bc58f345f8fa9da9, []int{0}
}

func (m *CreateServiceRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CreateServiceRequest.Unmarshal(m, b)
}
func (m *CreateServiceRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CreateServiceRequest.Marshal(b, m, deterministic)
}
func (m *CreateServiceRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CreateServiceRequest.Merge(m, src)
}
func (m *CreateServiceRequest) XXX_Size() int {
	return xxx_messageInfo_CreateServiceRequest.Size(m)
}
func (m *CreateServiceRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_CreateServiceRequest.DiscardUnknown(m)
}

var xxx_messageInfo_CreateServiceRequest proto.InternalMessageInfo

func (m *CreateServiceRequest) GetApiName() string {
	if m != nil {
		return m.ApiName
	}
	return ""
}

func (m *CreateServiceRequest) GetCreatedBy() string {
	if m != nil {
		return m.CreatedBy
	}
	return ""
}

func (m *CreateServiceRequest) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

type CreateServiceResponse struct {
	Uid                  string   `protobuf:"bytes,1,opt,name=uid,proto3" json:"uid,omitempty"`
	ApiName              string   `protobuf:"bytes,2,opt,name=api_name,json=apiName,proto3" json:"api_name,omitempty"`
	CreatedBy            string   `protobuf:"bytes,3,opt,name=created_by,json=createdBy,proto3" json:"created_by,omitempty"`
	Name                 string   `protobuf:"bytes,4,opt,name=name,proto3" json:"name,omitempty"`
	Type                 string   `protobuf:"bytes,5,opt,name=type,proto3" json:"type,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CreateServiceResponse) Reset()         { *m = CreateServiceResponse{} }
func (m *CreateServiceResponse) String() string { return proto.CompactTextString(m) }
func (*CreateServiceResponse) ProtoMessage()    {}
func (*CreateServiceResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_bc58f345f8fa9da9, []int{1}
}

func (m *CreateServiceResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CreateServiceResponse.Unmarshal(m, b)
}
func (m *CreateServiceResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CreateServiceResponse.Marshal(b, m, deterministic)
}
func (m *CreateServiceResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CreateServiceResponse.Merge(m, src)
}
func (m *CreateServiceResponse) XXX_Size() int {
	return xxx_messageInfo_CreateServiceResponse.Size(m)
}
func (m *CreateServiceResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_CreateServiceResponse.DiscardUnknown(m)
}

var xxx_messageInfo_CreateServiceResponse proto.InternalMessageInfo

func (m *CreateServiceResponse) GetUid() string {
	if m != nil {
		return m.Uid
	}
	return ""
}

func (m *CreateServiceResponse) GetApiName() string {
	if m != nil {
		return m.ApiName
	}
	return ""
}

func (m *CreateServiceResponse) GetCreatedBy() string {
	if m != nil {
		return m.CreatedBy
	}
	return ""
}

func (m *CreateServiceResponse) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *CreateServiceResponse) GetType() string {
	if m != nil {
		return m.Type
	}
	return ""
}

type CreateServiceEndpointRequest struct {
	ApiName              string   `protobuf:"bytes,1,opt,name=api_name,json=apiName,proto3" json:"api_name,omitempty"`
	CreatedBy            string   `protobuf:"bytes,2,opt,name=created_by,json=createdBy,proto3" json:"created_by,omitempty"`
	DbSchema             string   `protobuf:"bytes,3,opt,name=db_schema,json=dbSchema,proto3" json:"db_schema,omitempty"`
	Name                 string   `protobuf:"bytes,4,opt,name=name,proto3" json:"name,omitempty"`
	Service              string   `protobuf:"bytes,5,opt,name=service,proto3" json:"service,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CreateServiceEndpointRequest) Reset()         { *m = CreateServiceEndpointRequest{} }
func (m *CreateServiceEndpointRequest) String() string { return proto.CompactTextString(m) }
func (*CreateServiceEndpointRequest) ProtoMessage()    {}
func (*CreateServiceEndpointRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_bc58f345f8fa9da9, []int{2}
}

func (m *CreateServiceEndpointRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CreateServiceEndpointRequest.Unmarshal(m, b)
}
func (m *CreateServiceEndpointRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CreateServiceEndpointRequest.Marshal(b, m, deterministic)
}
func (m *CreateServiceEndpointRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CreateServiceEndpointRequest.Merge(m, src)
}
func (m *CreateServiceEndpointRequest) XXX_Size() int {
	return xxx_messageInfo_CreateServiceEndpointRequest.Size(m)
}
func (m *CreateServiceEndpointRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_CreateServiceEndpointRequest.DiscardUnknown(m)
}

var xxx_messageInfo_CreateServiceEndpointRequest proto.InternalMessageInfo

func (m *CreateServiceEndpointRequest) GetApiName() string {
	if m != nil {
		return m.ApiName
	}
	return ""
}

func (m *CreateServiceEndpointRequest) GetCreatedBy() string {
	if m != nil {
		return m.CreatedBy
	}
	return ""
}

func (m *CreateServiceEndpointRequest) GetDbSchema() string {
	if m != nil {
		return m.DbSchema
	}
	return ""
}

func (m *CreateServiceEndpointRequest) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *CreateServiceEndpointRequest) GetService() string {
	if m != nil {
		return m.Service
	}
	return ""
}

type CreateServiceEndpointResponse struct {
	Uid                  string   `protobuf:"bytes,1,opt,name=uid,proto3" json:"uid,omitempty"`
	ApiName              string   `protobuf:"bytes,2,opt,name=api_name,json=apiName,proto3" json:"api_name,omitempty"`
	CreatedBy            string   `protobuf:"bytes,3,opt,name=created_by,json=createdBy,proto3" json:"created_by,omitempty"`
	Name                 string   `protobuf:"bytes,4,opt,name=name,proto3" json:"name,omitempty"`
	Service              string   `protobuf:"bytes,5,opt,name=service,proto3" json:"service,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CreateServiceEndpointResponse) Reset()         { *m = CreateServiceEndpointResponse{} }
func (m *CreateServiceEndpointResponse) String() string { return proto.CompactTextString(m) }
func (*CreateServiceEndpointResponse) ProtoMessage()    {}
func (*CreateServiceEndpointResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_bc58f345f8fa9da9, []int{3}
}

func (m *CreateServiceEndpointResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CreateServiceEndpointResponse.Unmarshal(m, b)
}
func (m *CreateServiceEndpointResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CreateServiceEndpointResponse.Marshal(b, m, deterministic)
}
func (m *CreateServiceEndpointResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CreateServiceEndpointResponse.Merge(m, src)
}
func (m *CreateServiceEndpointResponse) XXX_Size() int {
	return xxx_messageInfo_CreateServiceEndpointResponse.Size(m)
}
func (m *CreateServiceEndpointResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_CreateServiceEndpointResponse.DiscardUnknown(m)
}

var xxx_messageInfo_CreateServiceEndpointResponse proto.InternalMessageInfo

func (m *CreateServiceEndpointResponse) GetUid() string {
	if m != nil {
		return m.Uid
	}
	return ""
}

func (m *CreateServiceEndpointResponse) GetApiName() string {
	if m != nil {
		return m.ApiName
	}
	return ""
}

func (m *CreateServiceEndpointResponse) GetCreatedBy() string {
	if m != nil {
		return m.CreatedBy
	}
	return ""
}

func (m *CreateServiceEndpointResponse) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *CreateServiceEndpointResponse) GetService() string {
	if m != nil {
		return m.Service
	}
	return ""
}

type CreateServiceEndpointProviderRequest struct {
	CreatedBy            string   `protobuf:"bytes,1,opt,name=created_by,json=createdBy,proto3" json:"created_by,omitempty"`
	MappedSchema         string   `protobuf:"bytes,2,opt,name=mapped_schema,json=mappedSchema,proto3" json:"mapped_schema,omitempty"`
	Name                 string   `protobuf:"bytes,3,opt,name=name,proto3" json:"name,omitempty"`
	NextPageField        string   `protobuf:"bytes,4,opt,name=next_page_field,json=nextPageField,proto3" json:"next_page_field,omitempty"`
	ServiceEndpoint      string   `protobuf:"bytes,5,opt,name=service_endpoint,json=serviceEndpoint,proto3" json:"service_endpoint,omitempty"`
	Url                  string   `protobuf:"bytes,6,opt,name=url,proto3" json:"url,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CreateServiceEndpointProviderRequest) Reset()         { *m = CreateServiceEndpointProviderRequest{} }
func (m *CreateServiceEndpointProviderRequest) String() string { return proto.CompactTextString(m) }
func (*CreateServiceEndpointProviderRequest) ProtoMessage()    {}
func (*CreateServiceEndpointProviderRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_bc58f345f8fa9da9, []int{4}
}

func (m *CreateServiceEndpointProviderRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CreateServiceEndpointProviderRequest.Unmarshal(m, b)
}
func (m *CreateServiceEndpointProviderRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CreateServiceEndpointProviderRequest.Marshal(b, m, deterministic)
}
func (m *CreateServiceEndpointProviderRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CreateServiceEndpointProviderRequest.Merge(m, src)
}
func (m *CreateServiceEndpointProviderRequest) XXX_Size() int {
	return xxx_messageInfo_CreateServiceEndpointProviderRequest.Size(m)
}
func (m *CreateServiceEndpointProviderRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_CreateServiceEndpointProviderRequest.DiscardUnknown(m)
}

var xxx_messageInfo_CreateServiceEndpointProviderRequest proto.InternalMessageInfo

func (m *CreateServiceEndpointProviderRequest) GetCreatedBy() string {
	if m != nil {
		return m.CreatedBy
	}
	return ""
}

func (m *CreateServiceEndpointProviderRequest) GetMappedSchema() string {
	if m != nil {
		return m.MappedSchema
	}
	return ""
}

func (m *CreateServiceEndpointProviderRequest) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *CreateServiceEndpointProviderRequest) GetNextPageField() string {
	if m != nil {
		return m.NextPageField
	}
	return ""
}

func (m *CreateServiceEndpointProviderRequest) GetServiceEndpoint() string {
	if m != nil {
		return m.ServiceEndpoint
	}
	return ""
}

func (m *CreateServiceEndpointProviderRequest) GetUrl() string {
	if m != nil {
		return m.Url
	}
	return ""
}

type CreateServiceEndpointProviderResponse struct {
	Uid                  string   `protobuf:"bytes,1,opt,name=uid,proto3" json:"uid,omitempty"`
	CreatedBy            string   `protobuf:"bytes,2,opt,name=created_by,json=createdBy,proto3" json:"created_by,omitempty"`
	Name                 string   `protobuf:"bytes,3,opt,name=name,proto3" json:"name,omitempty"`
	ServiceEndpoint      string   `protobuf:"bytes,4,opt,name=service_endpoint,json=serviceEndpoint,proto3" json:"service_endpoint,omitempty"`
	Url                  string   `protobuf:"bytes,5,opt,name=url,proto3" json:"url,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CreateServiceEndpointProviderResponse) Reset()         { *m = CreateServiceEndpointProviderResponse{} }
func (m *CreateServiceEndpointProviderResponse) String() string { return proto.CompactTextString(m) }
func (*CreateServiceEndpointProviderResponse) ProtoMessage()    {}
func (*CreateServiceEndpointProviderResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_bc58f345f8fa9da9, []int{5}
}

func (m *CreateServiceEndpointProviderResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CreateServiceEndpointProviderResponse.Unmarshal(m, b)
}
func (m *CreateServiceEndpointProviderResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CreateServiceEndpointProviderResponse.Marshal(b, m, deterministic)
}
func (m *CreateServiceEndpointProviderResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CreateServiceEndpointProviderResponse.Merge(m, src)
}
func (m *CreateServiceEndpointProviderResponse) XXX_Size() int {
	return xxx_messageInfo_CreateServiceEndpointProviderResponse.Size(m)
}
func (m *CreateServiceEndpointProviderResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_CreateServiceEndpointProviderResponse.DiscardUnknown(m)
}

var xxx_messageInfo_CreateServiceEndpointProviderResponse proto.InternalMessageInfo

func (m *CreateServiceEndpointProviderResponse) GetUid() string {
	if m != nil {
		return m.Uid
	}
	return ""
}

func (m *CreateServiceEndpointProviderResponse) GetCreatedBy() string {
	if m != nil {
		return m.CreatedBy
	}
	return ""
}

func (m *CreateServiceEndpointProviderResponse) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *CreateServiceEndpointProviderResponse) GetServiceEndpoint() string {
	if m != nil {
		return m.ServiceEndpoint
	}
	return ""
}

func (m *CreateServiceEndpointProviderResponse) GetUrl() string {
	if m != nil {
		return m.Url
	}
	return ""
}

type GetServicesRequest struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GetServicesRequest) Reset()         { *m = GetServicesRequest{} }
func (m *GetServicesRequest) String() string { return proto.CompactTextString(m) }
func (*GetServicesRequest) ProtoMessage()    {}
func (*GetServicesRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_bc58f345f8fa9da9, []int{6}
}

func (m *GetServicesRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetServicesRequest.Unmarshal(m, b)
}
func (m *GetServicesRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetServicesRequest.Marshal(b, m, deterministic)
}
func (m *GetServicesRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetServicesRequest.Merge(m, src)
}
func (m *GetServicesRequest) XXX_Size() int {
	return xxx_messageInfo_GetServicesRequest.Size(m)
}
func (m *GetServicesRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_GetServicesRequest.DiscardUnknown(m)
}

var xxx_messageInfo_GetServicesRequest proto.InternalMessageInfo

type GetServicesResponse struct {
	Data                 string   `protobuf:"bytes,1,opt,name=data,proto3" json:"data,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GetServicesResponse) Reset()         { *m = GetServicesResponse{} }
func (m *GetServicesResponse) String() string { return proto.CompactTextString(m) }
func (*GetServicesResponse) ProtoMessage()    {}
func (*GetServicesResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_bc58f345f8fa9da9, []int{7}
}

func (m *GetServicesResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetServicesResponse.Unmarshal(m, b)
}
func (m *GetServicesResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetServicesResponse.Marshal(b, m, deterministic)
}
func (m *GetServicesResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetServicesResponse.Merge(m, src)
}
func (m *GetServicesResponse) XXX_Size() int {
	return xxx_messageInfo_GetServicesResponse.Size(m)
}
func (m *GetServicesResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_GetServicesResponse.DiscardUnknown(m)
}

var xxx_messageInfo_GetServicesResponse proto.InternalMessageInfo

func (m *GetServicesResponse) GetData() string {
	if m != nil {
		return m.Data
	}
	return ""
}

type GetServiceEndpointDataRequest struct {
	ServiceEndpoint      string   `protobuf:"bytes,1,opt,name=service_endpoint,json=serviceEndpoint,proto3" json:"service_endpoint,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GetServiceEndpointDataRequest) Reset()         { *m = GetServiceEndpointDataRequest{} }
func (m *GetServiceEndpointDataRequest) String() string { return proto.CompactTextString(m) }
func (*GetServiceEndpointDataRequest) ProtoMessage()    {}
func (*GetServiceEndpointDataRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_bc58f345f8fa9da9, []int{8}
}

func (m *GetServiceEndpointDataRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetServiceEndpointDataRequest.Unmarshal(m, b)
}
func (m *GetServiceEndpointDataRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetServiceEndpointDataRequest.Marshal(b, m, deterministic)
}
func (m *GetServiceEndpointDataRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetServiceEndpointDataRequest.Merge(m, src)
}
func (m *GetServiceEndpointDataRequest) XXX_Size() int {
	return xxx_messageInfo_GetServiceEndpointDataRequest.Size(m)
}
func (m *GetServiceEndpointDataRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_GetServiceEndpointDataRequest.DiscardUnknown(m)
}

var xxx_messageInfo_GetServiceEndpointDataRequest proto.InternalMessageInfo

func (m *GetServiceEndpointDataRequest) GetServiceEndpoint() string {
	if m != nil {
		return m.ServiceEndpoint
	}
	return ""
}

type GetServiceEndpointDataResponse struct {
	Data                 string   `protobuf:"bytes,1,opt,name=data,proto3" json:"data,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GetServiceEndpointDataResponse) Reset()         { *m = GetServiceEndpointDataResponse{} }
func (m *GetServiceEndpointDataResponse) String() string { return proto.CompactTextString(m) }
func (*GetServiceEndpointDataResponse) ProtoMessage()    {}
func (*GetServiceEndpointDataResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_bc58f345f8fa9da9, []int{9}
}

func (m *GetServiceEndpointDataResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetServiceEndpointDataResponse.Unmarshal(m, b)
}
func (m *GetServiceEndpointDataResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetServiceEndpointDataResponse.Marshal(b, m, deterministic)
}
func (m *GetServiceEndpointDataResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetServiceEndpointDataResponse.Merge(m, src)
}
func (m *GetServiceEndpointDataResponse) XXX_Size() int {
	return xxx_messageInfo_GetServiceEndpointDataResponse.Size(m)
}
func (m *GetServiceEndpointDataResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_GetServiceEndpointDataResponse.DiscardUnknown(m)
}

var xxx_messageInfo_GetServiceEndpointDataResponse proto.InternalMessageInfo

func (m *GetServiceEndpointDataResponse) GetData() string {
	if m != nil {
		return m.Data
	}
	return ""
}

type CreateUserRequest struct {
	Email                string   `protobuf:"bytes,1,opt,name=email,proto3" json:"email,omitempty"`
	Name                 string   `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CreateUserRequest) Reset()         { *m = CreateUserRequest{} }
func (m *CreateUserRequest) String() string { return proto.CompactTextString(m) }
func (*CreateUserRequest) ProtoMessage()    {}
func (*CreateUserRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_bc58f345f8fa9da9, []int{10}
}

func (m *CreateUserRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CreateUserRequest.Unmarshal(m, b)
}
func (m *CreateUserRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CreateUserRequest.Marshal(b, m, deterministic)
}
func (m *CreateUserRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CreateUserRequest.Merge(m, src)
}
func (m *CreateUserRequest) XXX_Size() int {
	return xxx_messageInfo_CreateUserRequest.Size(m)
}
func (m *CreateUserRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_CreateUserRequest.DiscardUnknown(m)
}

var xxx_messageInfo_CreateUserRequest proto.InternalMessageInfo

func (m *CreateUserRequest) GetEmail() string {
	if m != nil {
		return m.Email
	}
	return ""
}

func (m *CreateUserRequest) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

type CreateUserResponse struct {
	Email                string   `protobuf:"bytes,1,opt,name=email,proto3" json:"email,omitempty"`
	Name                 string   `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Uid                  string   `protobuf:"bytes,3,opt,name=uid,proto3" json:"uid,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CreateUserResponse) Reset()         { *m = CreateUserResponse{} }
func (m *CreateUserResponse) String() string { return proto.CompactTextString(m) }
func (*CreateUserResponse) ProtoMessage()    {}
func (*CreateUserResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_bc58f345f8fa9da9, []int{11}
}

func (m *CreateUserResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CreateUserResponse.Unmarshal(m, b)
}
func (m *CreateUserResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CreateUserResponse.Marshal(b, m, deterministic)
}
func (m *CreateUserResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CreateUserResponse.Merge(m, src)
}
func (m *CreateUserResponse) XXX_Size() int {
	return xxx_messageInfo_CreateUserResponse.Size(m)
}
func (m *CreateUserResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_CreateUserResponse.DiscardUnknown(m)
}

var xxx_messageInfo_CreateUserResponse proto.InternalMessageInfo

func (m *CreateUserResponse) GetEmail() string {
	if m != nil {
		return m.Email
	}
	return ""
}

func (m *CreateUserResponse) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *CreateUserResponse) GetUid() string {
	if m != nil {
		return m.Uid
	}
	return ""
}

func init() {
	proto.RegisterType((*CreateServiceRequest)(nil), "crepoc.CreateServiceRequest")
	proto.RegisterType((*CreateServiceResponse)(nil), "crepoc.CreateServiceResponse")
	proto.RegisterType((*CreateServiceEndpointRequest)(nil), "crepoc.CreateServiceEndpointRequest")
	proto.RegisterType((*CreateServiceEndpointResponse)(nil), "crepoc.CreateServiceEndpointResponse")
	proto.RegisterType((*CreateServiceEndpointProviderRequest)(nil), "crepoc.CreateServiceEndpointProviderRequest")
	proto.RegisterType((*CreateServiceEndpointProviderResponse)(nil), "crepoc.CreateServiceEndpointProviderResponse")
	proto.RegisterType((*GetServicesRequest)(nil), "crepoc.GetServicesRequest")
	proto.RegisterType((*GetServicesResponse)(nil), "crepoc.GetServicesResponse")
	proto.RegisterType((*GetServiceEndpointDataRequest)(nil), "crepoc.GetServiceEndpointDataRequest")
	proto.RegisterType((*GetServiceEndpointDataResponse)(nil), "crepoc.GetServiceEndpointDataResponse")
	proto.RegisterType((*CreateUserRequest)(nil), "crepoc.CreateUserRequest")
	proto.RegisterType((*CreateUserResponse)(nil), "crepoc.CreateUserResponse")
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// CREServiceClient is the client API for CREService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type CREServiceClient interface {
	CreateService(ctx context.Context, in *CreateServiceRequest, opts ...grpc.CallOption) (*CreateServiceResponse, error)
	CreateServiceEndpoint(ctx context.Context, in *CreateServiceEndpointRequest, opts ...grpc.CallOption) (*CreateServiceEndpointResponse, error)
	CreateServiceEndpointProvider(ctx context.Context, in *CreateServiceEndpointProviderRequest, opts ...grpc.CallOption) (*CreateServiceEndpointProviderResponse, error)
	CreateUser(ctx context.Context, in *CreateUserRequest, opts ...grpc.CallOption) (*CreateUserResponse, error)
	GetServices(ctx context.Context, in *GetServicesRequest, opts ...grpc.CallOption) (*GetServicesResponse, error)
	GetServiceEndpointData(ctx context.Context, in *GetServiceEndpointDataRequest, opts ...grpc.CallOption) (CREService_GetServiceEndpointDataClient, error)
}

type cREServiceClient struct {
	cc *grpc.ClientConn
}

func NewCREServiceClient(cc *grpc.ClientConn) CREServiceClient {
	return &cREServiceClient{cc}
}

func (c *cREServiceClient) CreateService(ctx context.Context, in *CreateServiceRequest, opts ...grpc.CallOption) (*CreateServiceResponse, error) {
	out := new(CreateServiceResponse)
	err := c.cc.Invoke(ctx, "/crepoc.CREService/CreateService", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *cREServiceClient) CreateServiceEndpoint(ctx context.Context, in *CreateServiceEndpointRequest, opts ...grpc.CallOption) (*CreateServiceEndpointResponse, error) {
	out := new(CreateServiceEndpointResponse)
	err := c.cc.Invoke(ctx, "/crepoc.CREService/CreateServiceEndpoint", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *cREServiceClient) CreateServiceEndpointProvider(ctx context.Context, in *CreateServiceEndpointProviderRequest, opts ...grpc.CallOption) (*CreateServiceEndpointProviderResponse, error) {
	out := new(CreateServiceEndpointProviderResponse)
	err := c.cc.Invoke(ctx, "/crepoc.CREService/CreateServiceEndpointProvider", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *cREServiceClient) CreateUser(ctx context.Context, in *CreateUserRequest, opts ...grpc.CallOption) (*CreateUserResponse, error) {
	out := new(CreateUserResponse)
	err := c.cc.Invoke(ctx, "/crepoc.CREService/CreateUser", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *cREServiceClient) GetServices(ctx context.Context, in *GetServicesRequest, opts ...grpc.CallOption) (*GetServicesResponse, error) {
	out := new(GetServicesResponse)
	err := c.cc.Invoke(ctx, "/crepoc.CREService/GetServices", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *cREServiceClient) GetServiceEndpointData(ctx context.Context, in *GetServiceEndpointDataRequest, opts ...grpc.CallOption) (CREService_GetServiceEndpointDataClient, error) {
	stream, err := c.cc.NewStream(ctx, &_CREService_serviceDesc.Streams[0], "/crepoc.CREService/GetServiceEndpointData", opts...)
	if err != nil {
		return nil, err
	}
	x := &cREServiceGetServiceEndpointDataClient{stream}
	if err := x.ClientStream.SendMsg(in); err != nil {
		return nil, err
	}
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	return x, nil
}

type CREService_GetServiceEndpointDataClient interface {
	Recv() (*GetServiceEndpointDataResponse, error)
	grpc.ClientStream
}

type cREServiceGetServiceEndpointDataClient struct {
	grpc.ClientStream
}

func (x *cREServiceGetServiceEndpointDataClient) Recv() (*GetServiceEndpointDataResponse, error) {
	m := new(GetServiceEndpointDataResponse)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

// CREServiceServer is the server API for CREService service.
type CREServiceServer interface {
	CreateService(context.Context, *CreateServiceRequest) (*CreateServiceResponse, error)
	CreateServiceEndpoint(context.Context, *CreateServiceEndpointRequest) (*CreateServiceEndpointResponse, error)
	CreateServiceEndpointProvider(context.Context, *CreateServiceEndpointProviderRequest) (*CreateServiceEndpointProviderResponse, error)
	CreateUser(context.Context, *CreateUserRequest) (*CreateUserResponse, error)
	GetServices(context.Context, *GetServicesRequest) (*GetServicesResponse, error)
	GetServiceEndpointData(*GetServiceEndpointDataRequest, CREService_GetServiceEndpointDataServer) error
}

func RegisterCREServiceServer(s *grpc.Server, srv CREServiceServer) {
	s.RegisterService(&_CREService_serviceDesc, srv)
}

func _CREService_CreateService_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateServiceRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CREServiceServer).CreateService(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/crepoc.CREService/CreateService",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CREServiceServer).CreateService(ctx, req.(*CreateServiceRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _CREService_CreateServiceEndpoint_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateServiceEndpointRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CREServiceServer).CreateServiceEndpoint(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/crepoc.CREService/CreateServiceEndpoint",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CREServiceServer).CreateServiceEndpoint(ctx, req.(*CreateServiceEndpointRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _CREService_CreateServiceEndpointProvider_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateServiceEndpointProviderRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CREServiceServer).CreateServiceEndpointProvider(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/crepoc.CREService/CreateServiceEndpointProvider",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CREServiceServer).CreateServiceEndpointProvider(ctx, req.(*CreateServiceEndpointProviderRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _CREService_CreateUser_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateUserRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CREServiceServer).CreateUser(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/crepoc.CREService/CreateUser",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CREServiceServer).CreateUser(ctx, req.(*CreateUserRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _CREService_GetServices_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetServicesRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CREServiceServer).GetServices(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/crepoc.CREService/GetServices",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CREServiceServer).GetServices(ctx, req.(*GetServicesRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _CREService_GetServiceEndpointData_Handler(srv interface{}, stream grpc.ServerStream) error {
	m := new(GetServiceEndpointDataRequest)
	if err := stream.RecvMsg(m); err != nil {
		return err
	}
	return srv.(CREServiceServer).GetServiceEndpointData(m, &cREServiceGetServiceEndpointDataServer{stream})
}

type CREService_GetServiceEndpointDataServer interface {
	Send(*GetServiceEndpointDataResponse) error
	grpc.ServerStream
}

type cREServiceGetServiceEndpointDataServer struct {
	grpc.ServerStream
}

func (x *cREServiceGetServiceEndpointDataServer) Send(m *GetServiceEndpointDataResponse) error {
	return x.ServerStream.SendMsg(m)
}

var _CREService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "crepoc.CREService",
	HandlerType: (*CREServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "CreateService",
			Handler:    _CREService_CreateService_Handler,
		},
		{
			MethodName: "CreateServiceEndpoint",
			Handler:    _CREService_CreateServiceEndpoint_Handler,
		},
		{
			MethodName: "CreateServiceEndpointProvider",
			Handler:    _CREService_CreateServiceEndpointProvider_Handler,
		},
		{
			MethodName: "CreateUser",
			Handler:    _CREService_CreateUser_Handler,
		},
		{
			MethodName: "GetServices",
			Handler:    _CREService_GetServices_Handler,
		},
	},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "GetServiceEndpointData",
			Handler:       _CREService_GetServiceEndpointData_Handler,
			ServerStreams: true,
		},
	},
	Metadata: "crepb/cre.proto",
}

func init() { proto.RegisterFile("crepb/cre.proto", fileDescriptor_bc58f345f8fa9da9) }

var fileDescriptor_bc58f345f8fa9da9 = []byte{
	// 582 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xbc, 0x95, 0xcf, 0x6e, 0xd3, 0x4e,
	0x10, 0xc7, 0x7f, 0x9b, 0xbf, 0xcd, 0xfc, 0x88, 0x52, 0x96, 0x80, 0x52, 0xb7, 0x41, 0xc8, 0x34,
	0x15, 0x95, 0x20, 0x20, 0xe0, 0xca, 0xa5, 0x25, 0x80, 0x38, 0x54, 0x51, 0x2a, 0x2e, 0x5c, 0xac,
	0x8d, 0x77, 0x5a, 0x2c, 0x12, 0x7b, 0xb1, 0xdd, 0x8a, 0x5c, 0x78, 0x01, 0x9e, 0x80, 0x33, 0x27,
	0x5e, 0x83, 0x57, 0xe1, 0x45, 0x90, 0xd7, 0xeb, 0xbf, 0x5d, 0x27, 0x45, 0x48, 0xdc, 0xd6, 0xb3,
	0x33, 0xfb, 0xfd, 0xec, 0xcc, 0xec, 0x18, 0x7a, 0xb6, 0x8f, 0x62, 0xfe, 0xd8, 0xf6, 0x71, 0x2c,
	0x7c, 0x2f, 0xf4, 0x68, 0x2b, 0x32, 0x78, 0xb6, 0xc9, 0xa1, 0x7f, 0xec, 0x23, 0x0b, 0xf1, 0x14,
	0xfd, 0x4b, 0xc7, 0xc6, 0x19, 0x7e, 0xba, 0xc0, 0x20, 0xa4, 0x3b, 0xb0, 0xc5, 0x84, 0x63, 0xb9,
	0x6c, 0x89, 0x03, 0x72, 0x8f, 0x3c, 0xe8, 0xcc, 0xda, 0x4c, 0x38, 0x27, 0x6c, 0x89, 0x74, 0x08,
	0x60, 0xcb, 0x10, 0x6e, 0xcd, 0x57, 0x83, 0x9a, 0xdc, 0xec, 0x28, 0xcb, 0xd1, 0x8a, 0x52, 0x68,
	0xc8, 0xa8, 0xba, 0xdc, 0x90, 0x6b, 0xf3, 0x2b, 0x81, 0xdb, 0x25, 0x99, 0x40, 0x78, 0x6e, 0x80,
	0x74, 0x1b, 0xea, 0x17, 0x0e, 0x57, 0x12, 0xd1, 0xb2, 0xa0, 0x5c, 0x5b, 0xa7, 0x5c, 0xaf, 0x52,
	0x6e, 0x64, 0xca, 0x91, 0x2d, 0x5c, 0x09, 0x1c, 0x34, 0x63, 0x5b, 0xb4, 0x36, 0xbf, 0x13, 0xd8,
	0x2b, 0xd0, 0x4c, 0x5c, 0x2e, 0x3c, 0xc7, 0x0d, 0xff, 0xfe, 0xf2, 0xbb, 0xd0, 0xe1, 0x73, 0x2b,
	0xb0, 0x3f, 0xe0, 0x92, 0x29, 0xc0, 0x2d, 0x3e, 0x3f, 0x95, 0xdf, 0x5a, 0xbe, 0x01, 0xb4, 0x83,
	0x18, 0x42, 0x21, 0x26, 0x9f, 0xe6, 0x37, 0x02, 0xc3, 0x0a, 0xca, 0x7f, 0x94, 0xbb, 0x6a, 0xb6,
	0x5f, 0x04, 0xf6, 0xb5, 0x6c, 0x53, 0xdf, 0xbb, 0x74, 0x38, 0xfa, 0x49, 0x26, 0x8b, 0xaa, 0xa4,
	0xac, 0x7a, 0x1f, 0xba, 0x4b, 0x26, 0x04, 0xf2, 0x24, 0x65, 0x31, 0xf4, 0x8d, 0xd8, 0x58, 0x4a,
	0x5b, 0xae, 0xa1, 0xe8, 0x01, 0xf4, 0x5c, 0xfc, 0x1c, 0x5a, 0x82, 0x9d, 0xa3, 0x75, 0xe6, 0xe0,
	0x82, 0x2b, 0xf2, 0x6e, 0x64, 0x9e, 0xb2, 0x73, 0x7c, 0x15, 0x19, 0xe9, 0x21, 0x6c, 0x2b, 0x66,
	0x0b, 0x15, 0xa2, 0xba, 0x4b, 0x2f, 0x28, 0x92, 0xcb, 0x6c, 0xfa, 0x8b, 0x41, 0x4b, 0x65, 0xd3,
	0x5f, 0x98, 0x3f, 0x08, 0x8c, 0x36, 0xdc, 0xb2, 0xb2, 0x12, 0x7f, 0xfe, 0x48, 0xb4, 0xac, 0x8d,
	0xb5, 0xac, 0xcd, 0x8c, 0xb5, 0x0f, 0xf4, 0x35, 0x86, 0x8a, 0x33, 0x50, 0xe9, 0x37, 0x0f, 0xe1,
	0x56, 0xc1, 0xaa, 0x70, 0x29, 0x34, 0x38, 0x0b, 0x99, 0xe2, 0x95, 0x6b, 0xf3, 0x2d, 0x0c, 0x33,
	0xd7, 0x44, 0xe8, 0x25, 0x0b, 0x59, 0x52, 0x4a, 0x1d, 0x1e, 0xd1, 0xe2, 0x99, 0xcf, 0xe1, 0x6e,
	0xd5, 0x59, 0x6b, 0x08, 0x5e, 0xc0, 0xcd, 0x38, 0xdb, 0xef, 0x82, 0xac, 0x81, 0xfa, 0xd0, 0xc4,
	0x25, 0x73, 0x16, 0xca, 0x33, 0xfe, 0x48, 0xd3, 0x57, 0xcb, 0xcd, 0x98, 0x29, 0xd0, 0x7c, 0xb8,
	0x12, 0xba, 0x76, 0x7c, 0x52, 0xc3, 0x7a, 0x5a, 0xc3, 0xa7, 0x3f, 0x1b, 0x00, 0xc7, 0xb3, 0x89,
	0xba, 0x07, 0x3d, 0x81, 0x6e, 0xa1, 0x1b, 0xe8, 0xde, 0x38, 0x1e, 0xa2, 0x63, 0xdd, 0x04, 0x35,
	0x86, 0x15, 0xbb, 0x31, 0x98, 0xf9, 0x1f, 0x3d, 0x2b, 0xcd, 0xc4, 0xb4, 0xba, 0xfb, 0xda, 0xc8,
	0xd2, 0x90, 0x32, 0x46, 0x1b, 0xbc, 0x52, 0x9d, 0x2f, 0x15, 0x73, 0x24, 0xe9, 0x62, 0xfa, 0x70,
	0xed, 0x49, 0xa5, 0x27, 0x6d, 0x3c, 0xba, 0xa6, 0x77, 0xaa, 0x3f, 0x01, 0xc8, 0x0a, 0x43, 0x77,
	0x8a, 0xe1, 0xb9, 0x5a, 0x1b, 0x86, 0x6e, 0x2b, 0x3d, 0xe6, 0x0d, 0xfc, 0x9f, 0xeb, 0x65, 0x9a,
	0x3a, 0x5f, 0x6d, 0x7b, 0x63, 0x57, 0xbb, 0x97, 0x9e, 0xf4, 0x11, 0xee, 0xe8, 0xdb, 0x93, 0x8e,
	0xae, 0x06, 0x6a, 0x9e, 0x82, 0x71, 0xb0, 0xc9, 0x2d, 0x91, 0x7a, 0x42, 0x8e, 0xda, 0xef, 0x9b,
	0xf2, 0xdf, 0x3b, 0x6f, 0xc9, 0x1f, 0xef, 0xb3, 0xdf, 0x01, 0x00, 0x00, 0xff, 0xff, 0x91, 0x8f,
	0xee, 0x4d, 0x8b, 0x07, 0x00, 0x00,
}
