/**
 * @fileoverview gRPC-Web generated client stub for crepoc
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!



const grpc = {};
grpc.web = require('grpc-web');

const proto = {};
proto.crepoc = require('./cre_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.crepoc.CREServiceClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.crepoc.CREServicePromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!proto.crepoc.CREServiceClient} The delegate callback based client
   */
  this.delegateClient_ = new proto.crepoc.CREServiceClient(
      hostname, credentials, options);

};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.crepoc.CreateServiceRequest,
 *   !proto.crepoc.CreateServiceResponse>}
 */
const methodInfo_CreateService = new grpc.web.AbstractClientBase.MethodInfo(
  proto.crepoc.CreateServiceResponse,
  /** @param {!proto.crepoc.CreateServiceRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.crepoc.CreateServiceResponse.deserializeBinary
);


/**
 * @param {!proto.crepoc.CreateServiceRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.crepoc.CreateServiceResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.crepoc.CreateServiceResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.crepoc.CREServiceClient.prototype.createService =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/crepoc.CREService/CreateService',
      request,
      metadata,
      methodInfo_CreateService,
      callback);
};


/**
 * @param {!proto.crepoc.CreateServiceRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.crepoc.CreateServiceResponse>}
 *     The XHR Node Readable Stream
 */
proto.crepoc.CREServicePromiseClient.prototype.createService =
    function(request, metadata) {
  return new Promise((resolve, reject) => {
    this.delegateClient_.createService(
      request, metadata, (error, response) => {
        error ? reject(error) : resolve(response);
      });
  });
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.crepoc.CreateServiceEndpointRequest,
 *   !proto.crepoc.CreateServiceEndpointResponse>}
 */
const methodInfo_CreateServiceEndpoint = new grpc.web.AbstractClientBase.MethodInfo(
  proto.crepoc.CreateServiceEndpointResponse,
  /** @param {!proto.crepoc.CreateServiceEndpointRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.crepoc.CreateServiceEndpointResponse.deserializeBinary
);


/**
 * @param {!proto.crepoc.CreateServiceEndpointRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.crepoc.CreateServiceEndpointResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.crepoc.CreateServiceEndpointResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.crepoc.CREServiceClient.prototype.createServiceEndpoint =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/crepoc.CREService/CreateServiceEndpoint',
      request,
      metadata,
      methodInfo_CreateServiceEndpoint,
      callback);
};


/**
 * @param {!proto.crepoc.CreateServiceEndpointRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.crepoc.CreateServiceEndpointResponse>}
 *     The XHR Node Readable Stream
 */
proto.crepoc.CREServicePromiseClient.prototype.createServiceEndpoint =
    function(request, metadata) {
  return new Promise((resolve, reject) => {
    this.delegateClient_.createServiceEndpoint(
      request, metadata, (error, response) => {
        error ? reject(error) : resolve(response);
      });
  });
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.crepoc.CreateServiceEndpointProviderRequest,
 *   !proto.crepoc.CreateServiceEndpointProviderResponse>}
 */
const methodInfo_CreateServiceEndpointProvider = new grpc.web.AbstractClientBase.MethodInfo(
  proto.crepoc.CreateServiceEndpointProviderResponse,
  /** @param {!proto.crepoc.CreateServiceEndpointProviderRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.crepoc.CreateServiceEndpointProviderResponse.deserializeBinary
);


/**
 * @param {!proto.crepoc.CreateServiceEndpointProviderRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.crepoc.CreateServiceEndpointProviderResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.crepoc.CreateServiceEndpointProviderResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.crepoc.CREServiceClient.prototype.createServiceEndpointProvider =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/crepoc.CREService/CreateServiceEndpointProvider',
      request,
      metadata,
      methodInfo_CreateServiceEndpointProvider,
      callback);
};


/**
 * @param {!proto.crepoc.CreateServiceEndpointProviderRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.crepoc.CreateServiceEndpointProviderResponse>}
 *     The XHR Node Readable Stream
 */
proto.crepoc.CREServicePromiseClient.prototype.createServiceEndpointProvider =
    function(request, metadata) {
  return new Promise((resolve, reject) => {
    this.delegateClient_.createServiceEndpointProvider(
      request, metadata, (error, response) => {
        error ? reject(error) : resolve(response);
      });
  });
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.crepoc.CreateUserRequest,
 *   !proto.crepoc.CreateUserResponse>}
 */
const methodInfo_CreateUser = new grpc.web.AbstractClientBase.MethodInfo(
  proto.crepoc.CreateUserResponse,
  /** @param {!proto.crepoc.CreateUserRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.crepoc.CreateUserResponse.deserializeBinary
);


/**
 * @param {!proto.crepoc.CreateUserRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.crepoc.CreateUserResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.crepoc.CreateUserResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.crepoc.CREServiceClient.prototype.createUser =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/crepoc.CREService/CreateUser',
      request,
      metadata,
      methodInfo_CreateUser,
      callback);
};


/**
 * @param {!proto.crepoc.CreateUserRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.crepoc.CreateUserResponse>}
 *     The XHR Node Readable Stream
 */
proto.crepoc.CREServicePromiseClient.prototype.createUser =
    function(request, metadata) {
  return new Promise((resolve, reject) => {
    this.delegateClient_.createUser(
      request, metadata, (error, response) => {
        error ? reject(error) : resolve(response);
      });
  });
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.crepoc.GetServicesRequest,
 *   !proto.crepoc.GetServicesResponse>}
 */
const methodInfo_GetServices = new grpc.web.AbstractClientBase.MethodInfo(
  proto.crepoc.GetServicesResponse,
  /** @param {!proto.crepoc.GetServicesRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.crepoc.GetServicesResponse.deserializeBinary
);


/**
 * @param {!proto.crepoc.GetServicesRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.crepoc.GetServicesResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.crepoc.GetServicesResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.crepoc.CREServiceClient.prototype.getServices =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/crepoc.CREService/GetServices',
      request,
      metadata,
      methodInfo_GetServices,
      callback);
};


/**
 * @param {!proto.crepoc.GetServicesRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.crepoc.GetServicesResponse>}
 *     The XHR Node Readable Stream
 */
proto.crepoc.CREServicePromiseClient.prototype.getServices =
    function(request, metadata) {
  return new Promise((resolve, reject) => {
    this.delegateClient_.getServices(
      request, metadata, (error, response) => {
        error ? reject(error) : resolve(response);
      });
  });
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.crepoc.GetServiceEndpointDataRequest,
 *   !proto.crepoc.GetServiceEndpointDataResponse>}
 */
const methodInfo_GetServiceEndpointData = new grpc.web.AbstractClientBase.MethodInfo(
  proto.crepoc.GetServiceEndpointDataResponse,
  /** @param {!proto.crepoc.GetServiceEndpointDataRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.crepoc.GetServiceEndpointDataResponse.deserializeBinary
);


/**
 * @param {!proto.crepoc.GetServiceEndpointDataRequest} request The request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.crepoc.GetServiceEndpointDataResponse>}
 *     The XHR Node Readable Stream
 */
proto.crepoc.CREServiceClient.prototype.getServiceEndpointData =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/crepoc.CREService/GetServiceEndpointData',
      request,
      metadata,
      methodInfo_GetServiceEndpointData);
};


module.exports = proto.crepoc;

