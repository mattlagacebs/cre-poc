#!/bin/bash
export PATH=$PATH:$HOME/go/bin
go install
protoc crepb/cre.proto --go_out=plugins=grpc:.
protoc crepb/cre.proto --js_out=import_style=commonjs:. --grpc-web_out=import_style=commonjs,mode=grpcwebtext:.
